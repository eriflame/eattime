# This imports all the layers for "EatTimePrototype" into eattimeprototypeLayers1
ETLayers = Framer.Importer.load "imported/EatTimePrototype"
for layerGroupName of ETLayers
	window[layerGroupName] = ETLayers[layerGroupName]
	console.log(window[layerGroupName])
	
slide = (layer, xValue, yValue, time=.2) ->
	right = new Animation({
		layer: layer,
		properties: {x: xValue, y: yValue}
		time: time
	})
	right.start()
	
slideX = (layer, xValue, time=.2) ->
	slide(layer, xValue, 0)
slideY = (layer, yValue, time=.2) ->
	slide(layer, 0, yValue)
	
showOrHide = (layer, value) ->
	doHideShow = new Animation({
		layer: layer,
		properties: {opacity: value}
		time: 0
	})	
	doHideShow.start()
hide = (layer) ->
	showOrHide(layer, 0)
show = (layer) ->
	showOrHide(layer, 1)

toggle = (layerOff, opacityR) ->
	turnOff = new Animation({
		layer: layerOff,
		properties: {opacity: opacityR}
		time: .2	
	})
	turnOff.start()

curve1 = "spring(300, 20, 50)"

scaleUp = (layer, scaleValue, time) ->
	doScale = new Animation({
		layer: layer,
		properties: {scale: scaleValue, .15}
		time: time
		curve: curve1	
	})
	doScale.start()
	
##############################Sign In Screen Stuff#############################
signInBtn.on Events.Click, ->
	slideX(login, -1080)
# ##############################Sign In Screen Stuff#############################
##############################Home Screen Stuff#############################
menuOn.on Events.Click, ->
	currentReservations.animate({
		properties: {x: 700, blur: 45, opacity: .8},
		time: .5
	})
myNewRes.on Events.Click, ->
	slideX(currentReservations, 1090)
	slideX(menu, 1090)
	
##############################Home Screen Stuff#############################
##############################Select Restaurant Stuff#############################
pickedDest3.on Events.Click, ->
	slideX(selectDest, -1080)

# ##############################Select Restaurant Stuff#############################

############################## Making Res Stuff#############################
hide(actualTime)
hide(timePicker)
slide(timePicker, 0, -120)
hide(mySelection)
hide(placeBotBar)

time.on Events.Click, ->
	show(timePicker)
	opacLayer.animate({
		properties: {blur: 100, opacity: .6}
		time: .3	
	})
	reservations.animate({
		properties: {blur: 45}
		time: .5
	})
	
	
mySelection.on Events.Click, ->
	show(mySelection)

finishTime.on Events.Click, ->
	hide(timePicker)
	hide(selectTimes)
	show(actualTime)
	reservations.animate({
		properties: {blur: 0}
		time: .4
	})
	show(placeBotBar)

placeBotBar.on Events.Click, ->
	slideX(reservations, -1080)
# ##############################Making Res Stuff#############################
# ##############################Invite Friends Stuff#############################
hide(selectElz)
hide(elzFill)
hide(epFill)
hide(epSelect)
hide(botBar)

person4.on Events.Click, ->
	show(epSelect)
	toggle(epFill, 1)
	scaleUp(epFill, 1.3, .15)
	show(botBar)
	hide(elizLin)
person.on Events.Click, ->
	show(selectElz)
	toggle(elzFill, 1)
	scaleUp(elzFill, 1.3, .15)
	show(elizLin)

send.on Events.Click, ->
	slideX(contact, -1080)
# ##############################Invite Friends Stuff#############################
