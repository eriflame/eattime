window.__imported__ = window.__imported__ || {};
window.__imported__["EatTimeAppleWatch/layers.json.js"] = [
	{
		"id": 734,
		"name": "waiterInfo",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/waiterInfo.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 728,
				"name": "person3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/person3.png",
					"frame": {
						"x": 89,
						"y": 75,
						"width": 163,
						"height": 162
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1535041008"
			}
		],
		"modification": "297601676"
	},
	{
		"id": 510,
		"name": "options",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/options.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 692,
				"name": "check copy 3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/check copy 3.png",
					"frame": {
						"x": 205,
						"y": 201,
						"width": 86,
						"height": 121
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1066754096"
			},
			{
				"id": 679,
				"name": "check copy 2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/check copy 2.png",
					"frame": {
						"x": 209,
						"y": 68,
						"width": 77,
						"height": 99
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1960592651"
			},
			{
				"id": 666,
				"name": "check copy",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/check copy.png",
					"frame": {
						"x": 49,
						"y": 213,
						"width": 77,
						"height": 101
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1934938612"
			},
			{
				"id": 535,
				"name": "check",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/check.png",
					"frame": {
						"x": 34,
						"y": 66,
						"width": 106,
						"height": 101
					}
				},
				"imageType": "png",
				"children": [
					{
						"id": 576,
						"name": "checkOrder",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": null,
						"imageType": null,
						"children": [
							{
								"id": 575,
								"name": "checkFill",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 333,
									"height": 379
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkFill.png",
									"frame": {
										"x": 77,
										"y": 178,
										"width": 25,
										"height": 23
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "2017175076"
							},
							{
								"id": 571,
								"name": "checkUn",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 333,
									"height": 379
								},
								"maskFrame": null,
								"image": {
									"path": "images/checkUn.png",
									"frame": {
										"x": 77,
										"y": 178,
										"width": 25,
										"height": 23
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "1888619018"
							}
						],
						"modification": "1497778832"
					}
				],
				"modification": "1317709567"
			}
		],
		"modification": "1363249254"
	},
	{
		"id": 354,
		"name": "friendsArrived",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/friendsArrived.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 374,
				"name": "header",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 372,
						"name": "nav",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/nav.png",
							"frame": {
								"x": 90,
								"y": 61,
								"width": 165,
								"height": 18
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "753662734"
					}
				],
				"modification": "1186435531"
			},
			{
				"id": 362,
				"name": "person2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/person2.png",
					"frame": {
						"x": 182,
						"y": 108,
						"width": 81,
						"height": 104
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "203923348"
			},
			{
				"id": 351,
				"name": "person1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/person1.png",
					"frame": {
						"x": 74,
						"y": 108,
						"width": 81,
						"height": 104
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2143469528"
			}
		],
		"modification": "318176665"
	},
	{
		"id": 755,
		"name": "navigation",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 751,
				"name": "highlight",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/highlight.png",
					"frame": {
						"x": 144,
						"y": 332,
						"width": 10,
						"height": 10
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1888708354"
			},
			{
				"id": 754,
				"name": "page3",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/page3.png",
					"frame": {
						"x": 186,
						"y": 332,
						"width": 10,
						"height": 10
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "753662735"
			},
			{
				"id": 745,
				"name": "page1",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/page1.png",
					"frame": {
						"x": 145,
						"y": 332,
						"width": 10,
						"height": 10
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "991970861"
			},
			{
				"id": 741,
				"name": "page2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/page2.png",
					"frame": {
						"x": 166,
						"y": 332,
						"width": 10,
						"height": 10
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1888708328"
			}
		],
		"modification": "890290558"
	},
	{
		"id": 294,
		"name": "fArrived",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/fArrived.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 315,
				"name": "okay",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/okay.png",
					"frame": {
						"x": 82,
						"y": 254,
						"width": 179,
						"height": 48
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1041530289"
			},
			{
				"id": 311,
				"name": "person3-2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": {
					"path": "images/person3-2.png",
					"frame": {
						"x": 89,
						"y": 75,
						"width": 163,
						"height": 162
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1039031780"
			}
		],
		"modification": "484875706"
	},
	{
		"id": 492,
		"name": "reminder",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/reminder.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 491,
				"name": "finish",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 486,
						"name": "accept",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/accept.png",
							"frame": {
								"x": 140,
								"y": 196,
								"width": 78,
								"height": 78
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "488371802"
					}
				],
				"modification": "1446732472"
			}
		],
		"modification": "1118094067"
	},
	{
		"id": 413,
		"name": "arrived",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 333,
			"height": 379
		},
		"maskFrame": null,
		"image": {
			"path": "images/arrived.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 333,
				"height": 379
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 421,
				"name": "finish-2",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 333,
					"height": 379
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 459,
						"name": "reject",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/reject.png",
							"frame": {
								"x": 191,
								"y": 205,
								"width": 80,
								"height": 80
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "961339268"
					},
					{
						"id": 455,
						"name": "acceptMy",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 333,
							"height": 379
						},
						"maskFrame": null,
						"image": {
							"path": "images/acceptMy.png",
							"frame": {
								"x": 81,
								"y": 205,
								"width": 78,
								"height": 78
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "1339483325"
					}
				],
				"modification": "95990709"
			}
		],
		"modification": "1931778252"
	}
]