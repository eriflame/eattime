# This imports all the layers for "EatTimeAppleWatch" into eattimeapplewatchLayers
ETLayers = Framer.Importer.load "imported/EatTimeAppleWatch"

for layerGroupName of ETLayers
	window[layerGroupName] = ETLayers[layerGroupName]
	console.log(window[layerGroupName])
	
slide = (layer, xValue, yValue, time=.2) ->
	right = new Animation({
		layer: layer,
		properties: {x: xValue, y: yValue}
		time: time
	})
	right.start()
	
slideX = (layer, xValue, time=.2) ->
	slide(layer, xValue, 0)
slideY = (layer, yValue, time=.2) ->
	slide(layer, 0, yValue)
	
showOrHide = (layer, value) ->
	doHideShow = new Animation({
		layer: layer,
		properties: {opacity: value}
		time: 0
	})	
	doHideShow.start()
hide = (layer) ->
	showOrHide(layer, 0)
show = (layer) ->
	showOrHide(layer, 1)

toggle = (layerOff, opacityR) ->
	turnOff = new Animation({
		layer: layerOff,
		properties: {opacity: opacityR}
		time: .2	
	})
	turnOff.start()

curve1 = "spring(300, 20, 50)"

scaleUp = (layer, scaleValue, time) ->
	doScale = new Animation({
		layer: layer,
		properties: {scale: scaleValue, .15}
		time: time
		curve: curve1	
	})
	doScale.start()

acceptMy.on Events.Click, ->
	slideX(arrived, -339)

reminder.on Events.Click, ->
	slideX(reminder, -339)
fArrived.on Events.Click, ->
	slideX(fArrived, -339)

friendsArrived.on Events.Click, ->
	slideX(friendsArrived, -339)
	slideX(highlight,21)
hide(checkUn)
hide(checkFill)

checkOrder.on Events.Click, ->
	show(checkFill)
	
navigation.on Events.Click, ->
	slideX(options, -339)
	slideX(highlight, 42)
	
	
