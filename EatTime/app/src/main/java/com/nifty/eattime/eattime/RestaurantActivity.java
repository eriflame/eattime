package com.nifty.eattime.eattime;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.location.Location;
import android.content.Intent;
import android.widget.Button;

/**
 * Created by dmccapes on 12/9/14.
 */
public class RestaurantActivity extends BaseActivity {

    private SharedPreferences mData;
    private ListView mList;
    //Restaurant info = {Name,Address,Phone Number,Open Times}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        if (mActivityTitle != null) {
            mActivityTitle.setText(getResources().getString(R.string.restaurant_activity));
            mLeftTitle.setText("Cancel");
        }
        mLeftTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(getApplicationContext(),ProfileActivity.class);
                profileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(profileIntent);
                finish();
            }
        });

        initUI();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.restaurant, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUI() {

        mList = (ListView) findViewById(R.id.restaurant_list);

//        int numR = mData.getInt("numR", 0);
        int numR = 4;
        final Restaurant[] restaurantData = new Restaurant[numR];
        restaurantData[0] = new Restaurant("Gilman Grill", "+1 (510) 524-2532", "1300 4th St., Berkeley", R.drawable.yard_house);
        restaurantData[1] = new Restaurant("La Note", "+1 (510) 843-1535", "2377 Shattuck Ave, Berkeley", R.drawable.canteen);
        restaurantData[2] = new Restaurant("Jupiter", "+1 (510) 843-8277", "2181 Shattuck Ave, Berkeley", R.drawable.gourmet_sea);
        restaurantData[3] = new Restaurant("Cheese Board Pizza", "+1 (510) 549-3183", "1512 Shattuck Ave, Berkeley", R.drawable.shannon_rose);

        if (numR != 0) {
            ArrayAdapter adapter = new ArrayAdapterRestaurant(this, R.layout.item_restaurant, restaurantData);

            mList.setAdapter(adapter);
            mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Restaurant restaurant = restaurantData[i];
                    Intent reservation_intent = new Intent(getApplicationContext(), DateActivity.class);
                    reservation_intent.putExtra(Constants.RESTAURANT_NAME, restaurant.getName());
//                    reservation_intent.putExtra("restaurant_address", restaurant.getAddress());
//                    reservation_intent.putExtra("restaurant_phone", restaurant.getPhoneNumber());
//                    reservation_intent.putExtra("restaurant_image", restaurant.getImage());
                    startActivity(reservation_intent);
                }
            });
        }
    }


}
