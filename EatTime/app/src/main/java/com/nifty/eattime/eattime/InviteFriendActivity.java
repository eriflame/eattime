package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;


public class InviteFriendActivity extends BaseActivity {
    private static final String sFIREBASE_URL = "https://eat-time.firebaseio.com";

    private String musername;
    private Firebase mUserref;
    private Intent mNextActivityIntent;
    private ListView mList;
    private Contract[] mContract;
    private LinkedList<String> mInvitees;
    private ImageButton mSendButton;
    private TextView mInvitedText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);
        if (mActivityTitle != null) {
            mActivityTitle.setText(getResources().getString(R.string.invite));
            mLeftTitle.setText(getResources().getString(R.string.cancel));
        }
        mLeftTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(getApplicationContext(),ProfileActivity.class);
                profileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(profileIntent);
                finish();
            }
        });
        mInvitedText = (TextView)findViewById(R.id.selected);
        initUI();
        setupUsername();
        mSendButton = (ImageButton)findViewById(R.id.sendButton);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseManager = FirebaseManager.getInstance();
                mUserref = mFirebaseManager.mRootRef.child("users");
                String day, month, year, restaurant_name, time;
                day = getIntent().getExtras().getString(Constants.DAY);
                month = getIntent().getExtras().getString("month");
                year = getIntent().getExtras().getString("year");
                restaurant_name = getIntent().getExtras().getString("restaurant_name");
                time = getIntent().getExtras().getString("time");
                if(mInvitees.size() > 0){

                    ReservationItem new_reserv = new ReservationItem(day+"/"+month+"/"+year+", "+time, restaurant_name, (mInvitees.size()+1)+"", musername);
                    Object[] invitee = mInvitees.toArray();
                    for(int i=0; i < invitee.length; i++){
                        Log.i("User: ", invitee[i].toString());
                        mUserref.child(invitee[i].toString().replace(" ","")).child("invitation").push().setValue(new_reserv);
                    }
                    mUserref.child(musername.replace(" ","")).child("invitation").push().setValue(new_reserv);
                    mNextActivityIntent = new Intent(getApplicationContext(), ProfileActivity.class);
                    startActivity(mNextActivityIntent);
                    Toast.makeText(getApplicationContext(), "Invitation Sent", Toast.LENGTH_SHORT).show();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(), "Please select contacts", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void setupUsername() {
        SharedPreferences mData = this.getSharedPreferences(Constants.USER_INFO, MODE_PRIVATE);
        musername = mData.getString(Constants.DB_KEY_EMAIL, "");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_invite_friend, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void initUI(){
        mList = (ListView) findViewById(R.id.friendList);
        SharedPreferences mData = this.getSharedPreferences(Constants.CONTACT_INFO, MODE_PRIVATE);
        String curr_contacts = mData.getString(Constants.CONTACTS, "");
        int OpenBracket = curr_contacts.indexOf("[");
        int LastBracket = curr_contacts.lastIndexOf("]");
        curr_contacts=curr_contacts.substring(OpenBracket+1, LastBracket);
        String[] contacts = curr_contacts.split(",");

        Log.i("CONTACT LIST", curr_contacts);
        int numR = contacts.length;
        final int[] checked = new int[numR];
        mInvitees = new LinkedList<String>();
        mContract = new Contract[numR];
//        int[] photo = new int[]{
//                R.drawable.circle_photo_amy,
//                R.drawable.circle_photo_ngoc,
//                R.drawable.circle_photo_trung,
//                R.drawable.circle_photo_dylan,
//                R.drawable.circle_photo,
//                R.drawable.circle_photo_ashley
//                };
        for(int i = 0; i < numR; i ++){
            String temp = contacts[i].replace(" ","");
            if(temp.equals("Amy@gmail:com")){
                Log.i("EMAIL", temp);
                mContract[i] = new Contract(temp, R.drawable.circle_photo_amy);
            }else if(temp.equals("Ngoc@gmail:com")){
                Log.i("EMAIL", temp);
                mContract[i] = new Contract(temp, R.drawable.circle_photo_ngoc);
            }else if(temp.equals("Trung@gmail:com")){
                Log.i("EMAIL", temp);
                mContract[i] = new Contract(temp, R.drawable.circle_photo_trung);
            }else if(temp.equals("Dylan@gmail:com")){
                Log.i("EMAIL", temp);
                mContract[i] = new Contract(temp, R.drawable.circle_photo_dylan);
            }else if(temp.equals("Elizabeth@gmail:com")){
                Log.i("EMAIL", temp);
                mContract[i] = new Contract(temp, R.drawable.circle_photo);
            }else if(temp.equals("Ashley@gmail:com")){
                Log.i("EMAIL", temp);
                mContract[i] = new Contract(temp, R.drawable.circle_photo_ashley);
            }else{
                Log.i("EMAIL", temp);
                mContract[i] = new Contract(temp, R.drawable.generic);
            }

//            if(i < 6){
//                mContract[i] = new Contract(temp, photo[i]);
//            }else{
//                mContract[i] = new Contract(temp, R.drawable.generic);
//            }
        }
        if(numR != 0) {
            ArrayAdapter adapter = new ArrayAdapterContract(this, R.layout.contract_list, mContract);

            mList.setAdapter(adapter);
            mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                    Log.i("ListView", "Click " + i);
                    Contract item = (Contract)mList.getItemAtPosition(i);
                    if(checked[i] == 0){
                        checked[i] = 1;
                        String name = item.sname.split("@")[0];
                        mInvitees.push(item.sname);
                        ImageView check = (ImageView)view.findViewById(R.id.check);
                        check.setBackground(getResources().getDrawable(R.drawable.checked));
                    }else{
                        checked[i] = 0;
                        mInvitees.remove(item.sname);
                        ImageView check = (ImageView)view.findViewById(R.id.check);
                        check.setBackground(getResources().getDrawable(R.drawable.unchecked));
                    }
                    String selectedString = mInvitees.toString();
                    mInvitedText.setText("");
                    for(int j = 0; j < mInvitees.size(); j++){
                        mInvitedText.append(mInvitees.get(j).split("@")[0]);
                        if (!(j == mInvitees.size() -1)) {
                            mInvitedText.append(", ");
                        }
                    }
//                    int indexOfOpenBracket = selectedString.indexOf("[");
//                    int indexOfLastBracket = selectedString.lastIndexOf("]");
//                    mInvitedText.setText(selectedString.substring(indexOfOpenBracket+1, indexOfLastBracket));
//                    Toast.makeText(getApplicationContext(), restaurant_name+" "+month+"/"+day+"/"+year+" "+time, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
