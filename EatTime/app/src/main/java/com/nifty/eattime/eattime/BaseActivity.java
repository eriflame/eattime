package com.nifty.eattime.eattime;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by ngoctranfire on 12/9/14.
 */
public class BaseActivity extends Activity {
    protected TextView mActivityTitle = null;
    protected FirebaseManager mFirebaseManager;
    protected LinkedList<String> mContractList = new LinkedList<String>();
    protected SharedPreferences mData;
    protected TextView mLeftTitle = null;
    protected TextView mRightTitle = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        if (getActionBar() != null) {
            getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getActionBar().setCustomView(R.layout.eat_time_action_bar);
            mActivityTitle = (TextView) findViewById(R.id.activity_title);
            mLeftTitle = (TextView) findViewById(R.id.left_title_bar);
            mRightTitle = (TextView) findViewById(R.id.right_title_bar);
        }
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mData = this.getSharedPreferences(Constants.CONTACT_INFO, MODE_PRIVATE);
        String curr_contacts = mData.getString(Constants.CONTACTS, "");
        final Set<String> cur_set = new HashSet<String>(Arrays.asList(curr_contacts.split(",")));
        mFirebaseManager = FirebaseManager.getInstance();

        Firebase usersRef = mFirebaseManager.mRootRef.child("users");
        usersRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String uid = dataSnapshot.getKey();
                if (uid != null) {
                    if (!mContractList.contains(uid)) {
                        mContractList.add(uid);
                    }
                    Set<String> new_set = new HashSet<String>(mContractList);
                    if (!new_set.equals(cur_set)) {
                        SharedPreferences.Editor editPrefs = mData.edit();
                        editPrefs.putString(Constants.CONTACTS, new_set.toString());
                        Log.i("FireBase Contact saved", "The Data Token is: " + new_set.toString());
                        editPrefs.apply();
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}
