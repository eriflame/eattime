package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ngoctranfire on 12/9/14.
 */
public class FirebaseManager {

    private static FirebaseManager mFirebaseManager = null;
    public Firebase mRootRef = new Firebase(Constants.FIREBASE_URL);
    private static final String TAG = FirebaseManager.class.getCanonicalName();
    protected FirebaseManager() {
        //Protected because cannot create through constructor!
    }

    public static FirebaseManager getInstance() {
        if (mFirebaseManager == null) {
            mFirebaseManager = new FirebaseManager();
        }
        return mFirebaseManager;
    }
    /**
     *
     * @param activity - The activity passed in
     * @param email - the entered email
     * @param password - the entered password
     * This function creates a user and then authenticates them!
     */
    public void createUser(final Activity activity, final String email, final String password ) {
        mRootRef.createUser(email, password, new Firebase.ResultHandler() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "User creation successful!: With Email: " + email + ", Password: " + password);
                Intent profileIntent = new Intent();
                profileIntent.setClass(activity, ProfileActivity.class);
                profileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.create_success), Toast.LENGTH_LONG).show();
                activity.startActivity(profileIntent);
                mFirebaseManager.verifyLogin(activity, email, password, true);
                activity.finish();

            }

            @Override
            public void onError(FirebaseError firebaseError) {
                Log.i(TAG, "User creation error: " + firebaseError.getMessage());
                Toast.makeText(activity.getApplicationContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }
    /**
     *
     * @param activity - The activity passed in
     * @param email - the entered email
     * @param password - the entered password
     * @param isNewUser - whether or not the user is a newly registered user or not
     * This function verifies the login and password for login! It will return an error if there is an error
     * If they are a first time user, it store them in the database for the first time!
     */
    public void verifyLogin(final Activity activity, final String email, final String password, final boolean isNewUser) {
        mRootRef.authWithPassword(email, password, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                Log.i(TAG, "User ID: " + authData.getUid() + ", Provider: " + authData.getProvider());
                Intent toProfileIntent = new Intent();
                SharedPreferences pref = activity.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editPrefs = pref.edit();
                editPrefs.putString(Constants.AUTH_TOKEN, authData.getToken());
                editPrefs.putString(Constants.PROVIDER, authData.getProvider());
                editPrefs.putString(Constants.DB_KEY_EMAIL, email.replace(".",":"));
                Log.d(TAG, "The Data Token is: " + authData.getToken());
                editPrefs.apply();

                if (isNewUser) { //Add the user to the database if they are a new user!
                    Firebase usersRef = mFirebaseManager.mRootRef.child("users");
                    Map<String, Object> map = new HashMap<String, Object>();
                    if (authData.getProviderData().containsKey("id")) {
                        map.put(Constants.DB_KEY_PROVIDER_ID, authData.getProviderData().get("id").toString());
                    }
                    if (authData.getProviderData().containsKey(Constants.DB_KEY_DISPLAY_NAME)) {
                        map.put(Constants.DB_KEY_DISPLAY_NAME, authData.getProviderData().get(Constants.DB_KEY_DISPLAY_NAME).toString());
                    }
                    map.put(Constants.DB_KEY_FIRST_NAME, "");
                    map.put(Constants.DB_KEY_LAST_NAME, "");
                    map.put(Constants.DB_KEY_FRIENDS, "");
                    String imageFile;
                    Bitmap rand_image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.circle_photo_ngoc);
                    imageFile = encodeTobase64(rand_image);
                    if (email.equals("Ngoc@gmail.com")) {
                        Bitmap image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.circle_photo_ngoc);
                        imageFile = encodeTobase64(image);
                    }
                    if (email.equals("Amy@gmail.com")) {
                        Bitmap image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.circle_photo_amy);
                        imageFile = encodeTobase64(image);
                    }
                    if (email.equals("Ashley@gmail.com")) {
                        Bitmap image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.circle_photo_ashley);
                        imageFile = encodeTobase64(image);
                    }
                    if (email.equals("Trung@gmail.com")) {
                        Bitmap image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.circle_photo_trung);
                        imageFile = encodeTobase64(image);
                    }
                    if (email.equals("Dylan@gmail.com")) {
                        Bitmap image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.circle_photo_dylan);
                        imageFile = encodeTobase64(image);
                    }
                    if (email.equals("Elizabeth@gmail.com")) {
                        Bitmap image = BitmapFactory.decodeResource(activity.getResources(), R.drawable.circle_photo);
                        imageFile = encodeTobase64(image);
                    }
                    map.put(Constants.DB_KEY_PROFILE_PICTURE, imageFile);
                    map.put(Constants.DB_KEY_RESERVATIONS, "");
                    map.put(Constants.DB_KEY_EATEN_WITH, 0);
                    map.put(Constants.DB_KEY_TIME_SAVED, 0);
                    map.put(Constants.DB_KEY_RESERVATION_POINTS, 0);
                    map.put(Constants.DB_KEY_UID, authData.getUid());
                    String emailRef = email.replace(".", ":");
                    usersRef.child(emailRef).setValue(map);
                }
                toProfileIntent.setClass(activity, ProfileActivity.class);
                activity.startActivity(toProfileIntent);
                activity.finish();
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                Toast.makeText(activity.getApplicationContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void verifyLogin(final Activity activity, final String email, final String password) {
        verifyLogin(activity, email, password, false);
    }

    public static String encodeTobase64(Bitmap image)
    {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }
    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}
