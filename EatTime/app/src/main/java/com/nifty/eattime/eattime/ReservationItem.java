package com.nifty.eattime.eattime;

/**
 * Created by jinyusheng on 12/5/14.
 */
public class ReservationItem {
    public String time; //"Tuesday 4 - 5 pm"
    public String restName;
    public String people;
    public String inviter;

    public ReservationItem(String time, String restName, String people, String inviter){
        this.time = time;
        this.restName = restName;
        this.people = people;
        this.inviter = inviter;
    }
}