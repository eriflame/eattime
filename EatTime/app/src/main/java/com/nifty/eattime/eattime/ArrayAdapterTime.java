package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dmccapes on 12/11/14.
 */
public class ArrayAdapterTime extends ArrayAdapter {

    private Context mContext = null;
    private int mLayoutResourceId;
    String mData[] = null;

    public  ArrayAdapterTime(Context mContext, int layoutResourceId, String[] data){
        super(mContext, layoutResourceId, data);
        this.mLayoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, parent, false);
        }
        String time = mData[position];

        ((TextView) convertView.findViewById(R.id.tvTime)).setText(time);
        return convertView;

    }

}
