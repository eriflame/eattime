package com.nifty.eattime.eattime;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Calendar;
import java.util.Iterator;
import android.location.Location;
import android.graphics.Bitmap;

/**
 * Created by dmccapes on 12/9/14.
 */
public class Restaurant {

    private String name, address, phone_number;
    private String[] open_times;
    private Location location;
    private int restaurant_image;

    public Restaurant(String restaurant_name, String restaurant_address, String number, int image) {
        name = restaurant_name;
        address = restaurant_address;
        phone_number = number;
        restaurant_image = image;
    }

    public Restaurant(String restaurant_name, String restaurant_address,
                        String number, Location restaurant_location) {
        name = restaurant_name;
        address = restaurant_address;
        phone_number = number;
        location = restaurant_location;
    }

    public int getImage() { return restaurant_image; }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() { return phone_number; }

    public Location getLocation() { return location; }

    public String[] getReservationTimes() {
        return open_times;
    }

    public void setName(String new_name) {
        name = new_name;
    }

    public void setAddress (String new_address) {
        address = new_address;
    }

    public void setPhoneNumber (String new_number) {
        phone_number = new_number;
    }

    public void setLocation (Location new_location) {
        location = new_location;
    }

    public void setReservationTimes (String[] times) {
        open_times = times;
    }

}
