package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class LoginActivity extends BaseActivity {

    private EditText mEmailEditText;
    private EditText mPasswordEditText;

    private Button mSignInBtn;
    private Button mSignUpBtn;
    private static final String TAG = LoginActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        mEmailEditText = (EditText) this.findViewById(R.id.login_email_input);
        mPasswordEditText = (EditText) this.findViewById(R.id.login_password_input);
        mSignInBtn = (Button) this.findViewById(R.id.login_signInBtn);
        mSignUpBtn = (Button) this.findViewById(R.id.login_signUpBtn);
        setupSignIn();
        setupSignUp();

    }
    private void setupSignIn() {
        //Set Up SignInBtn
        this.mSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailText = mEmailEditText.getText().toString();
                String passwordText = mPasswordEditText.getText().toString();

                if (emailText.equals("") || passwordText.equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_email_password), Toast.LENGTH_LONG).show();
                } else {
                    mFirebaseManager.verifyLogin(LoginActivity.this, emailText, passwordText);
                }
            }
        });
    }

    private void setupSignUp() {
        //Set Up SignUpBtn
        this.mSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toSignupIntent = new Intent();
                toSignupIntent.setClass(LoginActivity.this, SignupActivity.class);
                startActivity(toSignupIntent);
//                finish();
                //Do Not Finish In Case The user Needs to go back...
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
