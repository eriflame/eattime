package com.nifty.eattime.eattime;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.Toast;
import android.app.Activity;
import android.widget.Button;
import android.content.Intent;

/**
 * Created by dmccapes on 12/10/14.
 */
public class DateActivity extends BaseActivity {

    CalendarView calendar;
    Button mSubmitBtn;
    String year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        initializeCalendar();
        mSubmitBtn = (Button) this.findViewById(R.id.date_submit);
        setSubmit();
        if (mActivityTitle != null) {
            mActivityTitle.setText(getResources().getString(R.string.pick_date));
            mLeftTitle.setText(getResources().getString(R.string.cancel));
        }
        mLeftTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(getApplicationContext(),ProfileActivity.class);
                profileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(profileIntent);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reservation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initializeCalendar() {
        calendar = (CalendarView) findViewById(R.id.date_calendar);
        calendar.setShowWeekNumber(false);
        calendar.setFirstDayOfWeek(2);
        calendar.setOnDateChangeListener(new OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int yr, int mnth, int dy) {
                year = Integer.toString(yr);
                month = Integer.toString(mnth);
                day = Integer.toString(dy);
            }
        });

    }

    public void setSubmit() {
        this.mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String restaurant_name = getIntent().getExtras().getString(Constants.RESTAURANT_NAME);
//                String restaurant_number = getIntent().getExtras().getString("restaurant_number");
//                String restaurant_address = getIntent().getExtras().getString("restaurant_address");
//                String restaurant_image = getIntent().getExtras().getString("restaurant_image");
                Intent toTimePickerIntent = new Intent();
                toTimePickerIntent.setClass(DateActivity.this, TimeActivity.class);
                toTimePickerIntent.putExtra(Constants.YEAR, year);
                toTimePickerIntent.putExtra(Constants.MONTH, month);
                toTimePickerIntent.putExtra(Constants.DAY, day);
                toTimePickerIntent.putExtra(Constants.RESTAURANT_NAME, restaurant_name);
//                toTimePickerIntent.putExtra("restaurant_number", restaurant_number);
//                toTimePickerIntent.putExtra("restaurant_address", restaurant_address);
//                toTimePickerIntent.putExtra("restaurant_image", restaurant_image);
                startActivity(toTimePickerIntent);
                finish();
            }
        });
    }
}
