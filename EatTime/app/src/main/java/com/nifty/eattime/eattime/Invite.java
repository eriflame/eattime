package com.nifty.eattime.eattime;

/**
 * Created by trungha on 12/9/14.
 */

public class Invite {

    private String message;
    private String author;

    // Required default constructor for Firebase object mapping
    @SuppressWarnings("unused")
    private Invite() { }

    Invite(String message, String author) {
        this.message = message;
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public String getAuthor() {
        return author;
    }
}
