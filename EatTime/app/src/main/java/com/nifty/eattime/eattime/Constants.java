package com.nifty.eattime.eattime;

/**
 * Created by ngoctranfire on 12/9/14.
 */
public class Constants {
    public static final String USER_INFO = "UserInfo";
    public static final String FIREBASE_URL = "https://eattime.firebaseio.com/";
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String PROVIDER = "PROVIDER";
    public static final String DB_KEY_EMAIL = "email";
    public static final String DB_KEY_FIRST_NAME = "firstName";
    public static final String DB_KEY_LAST_NAME = "lastName";
    public static final String DB_KEY_PROVIDER_ID = "providerID";
    public static final String DB_KEY_DISPLAY_NAME = "displayName";
    public static final String DB_KEY_FRIENDS = "friends";
    public static final String DB_KEY_PROFILE_PICTURE = "profilePicture";
    public static final String DB_KEY_RESERVATIONS = "reservations";
    public static final String DB_KEY_EATEN_WITH = "eatenWith";
    public static final String DB_KEY_TIME_SAVED = "timeSaved";
    public static final String DB_KEY_RESERVATION_POINTS = "reservationPoints";
    public static final String DB_KEY_UID = "UID";
    public static final String CONTACT_INFO = "ContactInfo";
    public static final String CONTACTS = "contacts";
    public static final String RESTAURANT_NAME = "restaurant_name";
    public static final String TIME = "time";
    public static final String DAY = "day";
    public static final String YEAR = "year";
    public static final String MONTH = "month";


}
