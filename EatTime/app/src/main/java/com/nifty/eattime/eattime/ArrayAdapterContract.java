package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by trungha on 12/9/14.
 */
public class ArrayAdapterContract extends ArrayAdapter<Contract>{
    private Context mContext = null;
    private int mLayoutResourceId;
    Contract mData[] = null;

    public  ArrayAdapterContract(Context mContext, int layoutResourceId, Contract[] data){
        super(mContext, layoutResourceId, data);
        this.mLayoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        ContractHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);

            holder = new ContractHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.photo);
            holder.txtTitle = (TextView)row.findViewById(R.id.name);

            row.setTag(holder);
        }
        else
        {
            holder = (ContractHolder)row.getTag();
        }

        Contract contract = mData[position];
        String short_name = (contract.sname).split("@")[0];

        holder.txtTitle.setText(short_name);
        holder.imgIcon.setImageResource(contract.photo);
        return row;
    }

    static class ContractHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
