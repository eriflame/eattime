package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by dmccapes on 12/10/14.
 */
public class ArrayAdapterRestaurant extends ArrayAdapter<Restaurant> {

    private Context mContext = null;
    private int mLayoutResourceId;
    Restaurant mData[] = null;

    public  ArrayAdapterRestaurant(Context mContext, int layoutResourceId, Restaurant[] data){
        super(mContext, layoutResourceId, data);
        this.mLayoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResourceId, parent, false);
        }
        Restaurant restaurant = mData[position];

        ((TextView) convertView.findViewById(R.id.restTVName)).setText(restaurant.getName());
        ((TextView) convertView.findViewById(R.id.restTVAddress)).setText(restaurant.getAddress());
        ((TextView) convertView.findViewById(R.id.restTVPhoneNumber)).setText(restaurant.getPhoneNumber());
        ((ImageView) convertView.findViewById(R.id.restTVBack)).setBackground(parent.getResources().getDrawable(restaurant.getImage()));
        return convertView;

    }

}
