package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;


public class WelcomeActivity extends BaseActivity {

    private SharedPreferences mData;
    private Intent mNextActivityIntent;
    private static final String TAG = WelcomeActivity.class.getCanonicalName();
    private Firebase mRootRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);

        setContentView(R.layout.activity_welcome);
        mData = this.getSharedPreferences(Constants.USER_INFO, MODE_PRIVATE);
        String authToken = mData.getString(Constants.AUTH_TOKEN, "");
        if (!authToken.equals("")) {
            Log.i(TAG, "Status: Logged in");
            mRootRef = new Firebase(Constants.FIREBASE_URL);
            mRootRef.authWithCustomToken(authToken, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    Log.d(TAG, "Authenticated with Welcome!");

//                    Using to log and test screen, after test put back to ProfileActivity
//                    mNextActivityIntent = new Intent(getApplicationContext(), InviteFriendActivity.class);
                    mNextActivityIntent = new Intent(getApplicationContext(), ProfileActivity.class);
                    startActivity(mNextActivityIntent);
                    finish();
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    Log.d("The Firebase Error: ", firebaseError.getMessage());
                    mNextActivityIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(mNextActivityIntent);
                    finish();

                }
            });
        } else {
            Log.i(TAG, "Status: Not logged in");
            mNextActivityIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(mNextActivityIntent);
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
