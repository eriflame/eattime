package com.nifty.eattime.eattime;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

/**
 * Created by dmccapes on 12/10/14.
 */
public class TimeActivity extends BaseActivity{

    String day, month, year, restaurant_name, restaurant_number, restaurant_address, restaurant_image;
    ListView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);
        day = getIntent().getExtras().getString(Constants.DAY);
        month = getIntent().getExtras().getString(Constants.MONTH);
        year = getIntent().getExtras().getString(Constants.YEAR);
        restaurant_name = getIntent().getExtras().getString(Constants.RESTAURANT_NAME);
//        restaurant_number = getIntent().getExtras().getString("restaurant_number");
//        restaurant_address = getIntent().getExtras().getString("restaurant_address");
//        restaurant_image = getIntent().getExtras().getString("restaurant_image");
        initUI();
        if (mActivityTitle != null) {
            mActivityTitle.setText(getResources().getString(R.string.pick_time));
            mLeftTitle.setText(getResources().getString(R.string.cancel));
        }
        mLeftTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(getApplicationContext(),ProfileActivity.class);
                profileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(profileIntent);
                finish();
            }
        });
        //Toast.makeText(getApplicationContext(),day+"/"+month+"/"+year+"/"+" "+restaurant_name,Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reservation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void initUI() {

        mList = (ListView) findViewById(R.id.time_list);

//        int numR = mData.getInt("numR", 0);
        final String[] timeData = new String[12];
        timeData[0] = "9:00 AM - 10:00AM";
        timeData[1] = "10:00 AM - 11:00AM";
        timeData[2] = "11:00 AM - 12:00PM";
        timeData[3] = "12:00 PM - 1:00PM";
        timeData[4] = "1:00 PM - 2:00PM";
        timeData[5] = "2:00 PM - 3:00PM";
        timeData[6] = "3:00 PM - 4:00PM";
        timeData[7] = "4:00 PM - 5:00PM";
        timeData[8] = "5:00 PM - 6:00PM";
        timeData[9] = "6:00 PM - 7:00PM";
        timeData[10] = "7:00 PM - 8:00PM";
        timeData[11] = "8:00 PM - 9:00PM";


        if (true) {
            ArrayAdapter adapter = new ArrayAdapterTime(this, R.layout.item_time_selector, timeData);

            mList.setAdapter(adapter);
            mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String time = timeData[i];
                    Intent toInviteIntent = new Intent();
                    toInviteIntent.setClass(TimeActivity.this, InviteFriendActivity.class);
                    toInviteIntent.putExtra("restaurant_name", restaurant_name);
                    toInviteIntent.putExtra("restaurant_number", restaurant_number);
                    toInviteIntent.putExtra("restaurant_address", restaurant_address);
                    toInviteIntent.putExtra("restaurant_image", restaurant_image);
                    toInviteIntent.putExtra("year", year);
                    toInviteIntent.putExtra("month", month);
                    toInviteIntent.putExtra("day", day);
                    toInviteIntent.putExtra("time", time);
                    startActivity(toInviteIntent);
                    finish();
                }
            });
        }
    }

}
