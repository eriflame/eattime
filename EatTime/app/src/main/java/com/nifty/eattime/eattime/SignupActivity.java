package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;


public class SignupActivity extends BaseActivity {

    private Context mContext;
    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private EditText mConfirmPWEditText;
    private Button mSignUpBtn;
    private Button mSignInBtn;
    private String TAG = SignupActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mContext = getApplicationContext();
        mEmailEditText = (EditText) this.findViewById(R.id.signup_email_input);
        mPasswordEditText = (EditText) this.findViewById(R.id.signup_password);
        mConfirmPWEditText = (EditText) this.findViewById(R.id.signup_confirm_password);
        setupSignUp();
        setupSignIn();
    }

    private void setupSignUp() {
        mSignUpBtn = (Button) this.findViewById(R.id.signup_signUpBtn);
        this.mSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userNameText = mEmailEditText.getText().toString();
                String passwordText = mPasswordEditText.getText().toString();
                String confirmPasswordText = mConfirmPWEditText.getText().toString();
                if (userNameText.equals("") || passwordText.equals("") || confirmPasswordText.matches("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_email_password), Toast.LENGTH_LONG).show();
                } else if (!passwordText.equals(confirmPasswordText)) {
                    Toast.makeText(mContext, getResources().getString(R.string.password_mismatch), Toast.LENGTH_LONG).show();
                } else {
                    mFirebaseManager.createUser(SignupActivity.this, userNameText, passwordText);
                }
            }
        });
    }

    private void setupSignIn() {
        mSignInBtn = (Button) this.findViewById(R.id.signup_signInBtn);

        this.mSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toLoginIntent = new Intent(SignupActivity.this, LoginActivity.class);
                toLoginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(toLoginIntent);
                finish();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
