package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jinyusheng on 12/5/14.
 */
public class ArrayAdapterItem extends ArrayAdapter<ReservationItem> {
    private Context mContext = null;
    private int mLayoutResourceId;
    ReservationItem mData[] = null;

    public  ArrayAdapterItem(Context mContext, int layoutResourceId, ReservationItem[] data){
        super(mContext, layoutResourceId, data);
        this.mLayoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView==null){

            // inflate the layout

            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

            convertView = inflater.inflate(mLayoutResourceId, parent, false);

        }
        ReservationItem reservationItem = mData[position];
        ImageView restPic = (ImageView) convertView.findViewById(R.id.restIV);
        String restName = reservationItem.restName;
        if(restName.equals("Gilman Grill")){
            restPic.setBackgroundResource(R.drawable.yard_house);
        }else if (restName.equals("La Note")){
            restPic.setBackgroundResource(R.drawable.canteen);
        }else if(restName.equals("Jupiter")){
            restPic.setBackgroundResource(R.drawable.gourmet_sea);
        }else if(restName.equals("Cheese Board Pizza")){
            restPic.setBackgroundResource(R.drawable.shannon_rose);
        }else{
            restPic.setBackgroundResource(R.drawable.og);
        }
        ((TextView) convertView.findViewById(R.id.timeText)).setText(reservationItem.time);
        ((TextView) convertView.findViewById(R.id.restNameText)).setText(reservationItem.restName);
        ((TextView) convertView.findViewById(R.id.peopleText)).setText(reservationItem.people);
        ((TextView) convertView.findViewById(R.id.inviterNameText)).setText(reservationItem.inviter);
        return convertView;

    }


}