package com.nifty.eattime.eattime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.*;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.DeckOfCardsLauncherIcon;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.util.ParcelableUtil;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Set;


public class ProfileActivity extends BaseActivity implements LocationListener {
    private SharedPreferences mData;
    private ListView mList;
    private CardImage mCardImage;
    private HashSet<ReservationItem> mReservation = new HashSet<ReservationItem>();


    //Toq Watch variables
    private final static String PREFS_FILE= "prefs_file";
    private final static String DECK_OF_CARDS_KEY= "deck_of_cards_key";
    private final static String DECK_OF_CARDS_VERSION_KEY= "deck_of_cards_version_key";
    private DeckOfCardsManager mDeckOfCardsManager;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;
    private ToqBroadcastReceiver toqReceiver;

    //Location-related variables
    protected LocationManager locationManager;
    protected Context context;
    protected Location here, restaurant_location;

    public String restaurant_name;

    /**
     * @see android.app.Activity#onStart()
     */
    protected void onStart(){
        super.onStart();
        install();
        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
//        mReservation = new HashSet<String>();
        if (mActivityTitle != null) {
            mActivityTitle.setText(getResources().getString(R.string.profile_activity));
            mLeftTitle.setText(getResources().getString(R.string.logout));
            mRightTitle.setText(getResources().getString(R.string.plus));
            mRightTitle.setTextSize(30);
            mRightTitle.setPadding(10, 0, 0, 0);

            mLeftTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent signInIntent = new Intent(getApplicationContext(), LoginActivity.class);
                    signInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(signInIntent);
                    finish();
                }
            });

            mRightTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent pickRestaurantIntent = new Intent(getApplicationContext(), RestaurantActivity.class);
                    startActivity(pickRestaurantIntent);
                }
            });
        }

        mData = this.getSharedPreferences(com.nifty.eattime.eattime.Constants.USER_INFO, MODE_PRIVATE);
        String loggedUser = mData.getString(com.nifty.eattime.eattime.Constants.DB_KEY_EMAIL, "");
        Firebase invitationref = mFirebaseManager.mRootRef.child("users").child(loggedUser.replace(" ","")).child("invitation");
        invitationref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //                Object new_inviter = dataSnapshot.getValue();
                ReservationItem new_reservation = new ReservationItem((String)(dataSnapshot.child("time").getValue()),(String)(dataSnapshot.child("restName").getValue()), (String)(dataSnapshot.child("people").getValue()) ,(String)(dataSnapshot.child("inviter").getValue()) );

                if(!mReservation.contains(new_reservation)){
                    Toast.makeText(getApplicationContext(), "New reservation received.", Toast.LENGTH_SHORT).show();
                    sendNotification((String)(dataSnapshot.child("restName").getValue()),"New reservation received", (String)(dataSnapshot.child("time").getValue()));
                }
                mReservation.add(new_reservation);
                initUI();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
        initUI();
        //Init Toq Watch variables
        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        init();
        toqReceiver = new ToqBroadcastReceiver();

        //Location-related variables
        // restaurant data is temporarily hardcoded as Chez Panisse and respective coordinates"
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 0, 0, this);

        here = new Location("here");
        restaurant_name = "Chez Panisse";
        restaurant_location = new Location(restaurant_name);
        restaurant_location.setLatitude(37.879816);
        restaurant_location.setLongitude(-122.268897);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.install_toq:
                install();
                return true;
            case R.id.uninstall_toq:
                uninstall();
                return true;
            case R.id.table_ready_notification:
                sendNotification("Alert","Reservation reminder" ,"Your table is ready!");
                return true;
            case R.id.arrival_notification:
                sendNotification("You have arrived at", "Reservation reminder",restaurant_name);
                return true;
            case R.id.friend_arrival_notification:
                sendNotification("Someone has arrived!", "Reservation reminder","Your friend FRIEND has arrived at RESTAURANT_NAME");
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initUI() {

        int numRev = mReservation.size();
        Log.i("START UI","START UI" + numRev);
        ReservationItem[] reservs = mReservation.toArray(new ReservationItem[numRev]);
        for (int i = 0; i < reservs.length; i++){
            Log.i("LIST OF RESERVATION", reservs[i].restName + " ");
        }

        mData = getSharedPreferences("UserInfo", MODE_PRIVATE);
        int saveTime = mData.getInt("savedtime", 0);
        int eatWith = mData.getInt("eatwith", 0);
        int points = mData.getInt("rpoints", 0);
        mList = (ListView) findViewById(R.id.listView);
//        ((TextView) this.findViewById(R.id.eatenWithText)).setText("Eaten With\n" + eatWith);
//        ((TextView) this.findViewById(R.id.timeSavesText)).setText("Time Saved\n" + saveTime);
//        ((TextView) this.findViewById(R.id.reservationPtsText)).setText("Reservation Pts\n" + points);

        if (reservs.length != 0) {
            ArrayAdapter adapter = new ArrayAdapterItem(this, R.layout.app_reservation_list, reservs);

            mList.setAdapter(adapter);
            mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                }
            });
        }
    }

        /**
         * Installs applet to Toq watch if app is not yet installed
         */
    private void install() {
        boolean isInstalled = true;

        try {
            mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }

        if (!isInstalled) {
            try {
                mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
            } catch (RemoteDeckOfCardsException e) {
                e.printStackTrace();
//                Toast.makeText(this, "Error: Cannot install application", Toast.LENGTH_SHORT).show();
            }
        } else {
//            Toast.makeText(this, "App is installed!", Toast.LENGTH_SHORT).show();
        }

        try{
            storeDeckOfCards();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void uninstall() {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }

        if (isInstalled) {
            try{
                mDeckOfCardsManager.uninstallDeckOfCards();
            }
            catch (RemoteDeckOfCardsException e){
//                Toast.makeText(this, getString(R.string.error_uninstalling_deck_of_cards), Toast.LENGTH_SHORT).show();
            }
        } else {
//            Toast.makeText(this, getString(R.string.already_uninstalled), Toast.LENGTH_SHORT).show();
        }
    }

    //    Initialise
    private void init(){
        // Create the resource store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();
        // Try to retrieve a stored deck of cards
        try {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        catch (Throwable th){
            th.printStackTrace();
        }

        DeckOfCardsLauncherIcon whiteIcon = null;
        DeckOfCardsLauncherIcon colorIcon = null;
        // Get the launcher icons
        try{
            whiteIcon= new DeckOfCardsLauncherIcon("white.launcher.icon", getBitmap("logo_bw.png"), DeckOfCardsLauncherIcon.WHITE);
            colorIcon= new DeckOfCardsLauncherIcon("color.launcher.icon", getBitmap("logo.png"), DeckOfCardsLauncherIcon.COLOR);
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Can't get launcher icon");
            return;
        }

        // Try to retrieve a stored deck of cards
        try {
            // If there is no stored deck of cards or it is unusable, then create new and store
            mRemoteDeckOfCards = createDeckOfCards();
//                    storeDeckOfCards();
        }
        catch (Throwable th){
            th.printStackTrace();
            mRemoteDeckOfCards = null; // Reset to force recreate
        }

        // Make sure in usable state
        if (mRemoteDeckOfCards == null){
            mRemoteDeckOfCards = createDeckOfCards();
        }
        // Set the custom launcher icons, adding them to the resource store
        mRemoteDeckOfCards.setLauncherIcons(mRemoteResourceStore, new DeckOfCardsLauncherIcon[]{whiteIcon, colorIcon});

    }

    private Bitmap getBitmap(String fileName) throws Exception{

        try{
            InputStream is= this.getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }

    // Create cards upon installation
    // For each contact invited, create the following cards:
    // 1. FRIEND arrived. 2. Remind FRIEND?
    private RemoteDeckOfCards createDeckOfCards() {
        ListCard listCard= new ListCard();
        SimpleTextCard simpleTextCard= new SimpleTextCard("card0");
        simpleTextCard.setHeaderText("Waitress Info");
        simpleTextCard.setShowDivider(true);
        String[] waitName = {"Waitress: James "};
        simpleTextCard.setMessageText(waitName);

        try {
            mCardImage = new CardImage("card.image.1", getBitmap("waitress.png"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        mRemoteResourceStore.addResource(mCardImage);
        simpleTextCard.setCardImage(mRemoteResourceStore, mCardImage);
        listCard.add(simpleTextCard);

        SimpleTextCard menu1 = new SimpleTextCard("card1");
        menu1.setHeaderText("Order");
        menu1.setShowDivider(true);
        String[] message = {"James will come to take your order soon!"};
        menu1.setMessageText(message);
        menu1.setTitleText("Ordering Option");
        listCard.add(menu1);

        SimpleTextCard menu2 = new SimpleTextCard("card2");
        menu2.setHeaderText("Check");
        menu2.setShowDivider(true);
        String[] message2 = {"James will come to take your check!"};
        menu2.setMessageText(message2);
        listCard.add(menu2);

        SimpleTextCard menu3 = new SimpleTextCard("card3");
        menu3.setHeaderText("Water");
        menu3.setShowDivider(true);
        String[] message3 = {"James will bring you water shortly!"};
        menu3.setMessageText(message3);
        listCard.add(menu3);

        SimpleTextCard menu4 = new SimpleTextCard("card4");
        menu4.setHeaderText("Help");
        menu4.setShowDivider(true);
        String[] message4 = {"James will assist you momentarily"};
        menu4.setMessageText(message4);
        listCard.add(menu4);
        return new RemoteDeckOfCards(this, listCard);
    }

    public void sendNotification(String title, String notif, String content) {
        String[] message = new String[2];
        message[0] = title;
        message[1] = notif;
        // Create a NotificationTextCard
        NotificationTextCard notificationCard = new NotificationTextCard(System.currentTimeMillis(),
                content, message);
        // Vibrate to alert user when showing the notification
        notificationCard.setVibeAlert(true);
        // Create a notification with the NotificationTextCard we made
        RemoteToqNotification notification = new RemoteToqNotification(this, notificationCard);

        try {
            // Send the notification
            mDeckOfCardsManager.sendNotification(notification);
//            Toast.makeText(this, "Sent Notification", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
//            Toast.makeText(this, "Failed to send Notification", Toast.LENGTH_SHORT).show();
        }

    }

    private RemoteDeckOfCards getStoredDeckOfCards() throws Exception{
        if (!isValidDeckOfCards()){
            //Log.w(Constants.TAG, "Stored deck of cards not valid for this version of the demo, recreating...");
            return null;
        }
        SharedPreferences prefs= getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        String deckOfCardsStr= prefs.getString(DECK_OF_CARDS_KEY, null);

        if (deckOfCardsStr == null){
            return null;
        }
        else{
            return ParcelableUtil.unmarshall(deckOfCardsStr, RemoteDeckOfCards.CREATOR);
        }
    }

    private void storeDeckOfCards() throws Exception{
        // Retrieve and hold the contents of PREFS_FILE, or create one when you retrieve an editor (SharedPreferences.edit())
        SharedPreferences prefs = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        // Create new editor with preferences above
        SharedPreferences.Editor editor = prefs.edit();
        // Store an encoded string of the deck of cards with key DECK_OF_CARDS_KEY
        editor.putString(DECK_OF_CARDS_KEY, ParcelableUtil.marshall(mRemoteDeckOfCards));
        // Store the version code with key DECK_OF_CARDS_VERSION_KEY
        editor.putInt(DECK_OF_CARDS_VERSION_KEY, com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants.VERSION_CODE);
        // Commit these changes
        editor.commit();
    }

    // Check if the stored deck of cards is valid for this version of the demo
    private boolean isValidDeckOfCards(){

        SharedPreferences prefs= getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        // Return 0 if DECK_OF_CARDS_VERSION_KEY isn't found
        int deckOfCardsVersion= prefs.getInt(DECK_OF_CARDS_VERSION_KEY, 0);

        return deckOfCardsVersion >= Constants.VERSION_CODE;
    }

    //Location methods
    @Override
    public void onLocationChanged(Location location) {
        here.setLatitude(location.getLatitude());
        here.setLongitude(location.getLongitude());

        if (here.distanceTo(restaurant_location) < 10) {
            ////trigger watch event when within 10 meters of restaurant location
            sendNotification("You have arrived at", "Reservation reminder",restaurant_name);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
